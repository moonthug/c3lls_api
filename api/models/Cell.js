/**
* Cell.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

    attributes: {
        title       : { type: 'string' },

        // Associations
        //members     : { collection: 'agent' },

        // Meta
        createdAt   : { type: 'date' },
        updatedAt   : { type: 'date' }
    }
};

