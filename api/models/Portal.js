/**
* Portal.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
      guid          : { type: 'string' },
      title         : { type: 'string' },
      location      : { type: 'array' },

      // Associations
      cell          : { model: 'cell' },

      // Meta
      createdAt     : { type: 'date' },
      updatedAt     : { type: 'date' }
  }
};

