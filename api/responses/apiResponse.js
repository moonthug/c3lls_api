/**
 * 200 (OK) Response
 *
 * Usage:
 * return res.sendApiSuccess();
 * return res.sendApiSuccess(data);
 *
 * @param  {Object} data
 */

module.exports = function sendApiResponse(err, data) {
    // Get access to `req`, `res`, & `sails`
    var req = this.req;
    var res = this.res;
    var sails = req._sails;

    sails.log.silly('res.sendApiSuccess() :: Sending 200 ("OK") response');

    if(!data) data = err;

    // Send Error
    if(err && typeof err == 'error') return res.serverError(err);

    // Set status code
    res.status(200);

    var responseJson = { success: true, payload: data };

    return res.json(responseJson);
};
