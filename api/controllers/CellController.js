/**
 * CellController
 *
 * @description :: Server-side logic for managing cells
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

    /**
     * `CellController.list()`
     */
    list: function(req, res) {
        Cell.find({}, res.apiResponse);
    }
};

