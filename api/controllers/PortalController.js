/**
 * PortalController
 *
 * @description :: Server-side logic for managing portals
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

    /**
     * `PortalController.create()`
     */
	create: function(req, res) {
        // Validate
        var portal = {
            guid        : req.body.guid,
            title       : req.body.title,
            location    : [parseInt(req.body.lat) / 1000000, parseInt(req.body.lng) / 1000000],
            cell        : req.body.cell
        };
        Portal.findOne({ guid: portal.guid }, function(err, doc) {
            if(err) return res.serverError(err);

            // Portal exists, just update
            if(doc) return Portal.update({ guid: portal.guid }, portal, res.apiResponse);

            Portal.create(portal, res.apiResponse);
        });
    },


    /**
     * `PortalController.update()`
     */
    update: function(req, res) {
        if(!req.params.guid) return res.notFound();

        Portal.update({ guid: req.params.guid }, req.body, function(err) {
            if(err) return res.serverError();

            Portal.find({ guid: portal.guid }, res.apiResponse);
        });
    },

    /**
     * `PortalController.list()`
     */
    list: function(req, res) {
        Portal.find({})
            .populate('cell')
            .exec(res.apiResponse);
    },

    /**
     * `PortalController.map()`
     */
    map: function(req, res) {
        var key = req.query.key || 'id';

        Portal.find({})
            .populate('cell')
            .exec(function(err, portals) {
                if(err) return res.serverError(err);

                var portalMap = {};
                portals.forEach(function(portal) {
                    portalMap[portal[key]] = portal;
                });

                res.apiResponse(portalMap);
            });
    }
};