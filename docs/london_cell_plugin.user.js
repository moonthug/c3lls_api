// ==UserScript==
// @name            London Cell Categorist
// @namespace       http://m00nplug.com/
// @version         0.1.3
// @category        Layer
// @updateURL       https://bitbucket.org/moonthug/c3lls_api/raw/HEAD/docs/london_cell_plugin.user.js
// @downloadURL     https://bitbucket.org/moonthug/c3lls_api/raw/HEAD/docs/london_cell_plugin.user.js
// @description     enter something useful
// @include         https://www.ingress.com/intel*
// @include         http://www.ingress.com/intel*
// @match           https://www.ingress.com/intel*
// @match           http://www.ingress.com/intel*
// @grant           none
// ==/UserScript==

function wrapper(plugin_info) {
// ensure plugin framework is there, even if iitc is not yet loaded
if(typeof window.plugin !== 'function') window.plugin = function() {};


// PLUGIN START ////////////////////////////////////////////////////////

///////////////////////////////////////
//
// Constructor
window.plugin.londonCellCategorist = function() {
};


///////////////////////////////////////
//
// Properties

// CONST
window.plugin.londonCellCategorist.apiUrl = 'https://localhost:1337/';

// UI
window.plugin.londonCellCategorist.markerLayers = {};
window.plugin.londonCellCategorist.markerLayersGroup = null;

// DATA
window.plugin.londonCellCategorist.cellList = [];
window.plugin.londonCellCategorist.portalMap = {};


///////////////////////////////////////
//
// Functions

window.plugin.londonCellCategorist.requestApi = function(method, resource, data, done) {
    var xmlhttp = new XMLHttpRequest();
    
    if(typeof data == 'function') done = data;
    
    xmlhttp.onload = function(e) {
        try {
            var response = JSON.parse(xmlhttp.responseText);
            done(null, response.payload);
        }
        catch(e) {
            //alert('Could not load cell list: ' + e.toString());
            done(e);
        }
    };
    
    xmlhttp.open(method, window.plugin.londonCellCategorist.apiUrl + resource, true);
    if(method.toLowerCase() == 'post') {
        xmlhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        var body = [];
        for(var key in data) {
          if(data.hasOwnProperty(key))
            body.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
        }
        xmlhttp.send(body.join('&'));
    }
    else
      xmlhttp.send();
};

window.plugin.londonCellCategorist.addPortal = function(portal) {
    var self = this;
    
    portal.latLng = L.latLng(portal.location[0], portal.location[1]);
    self.portalMap[portal.guid] = portal;
    
    var portalCellMarker = L.marker(portal.latLng, {
    	icon: L.icon({
            iconUrl         : 'http://www.poidb.com/images/Tick.gif',
            iconRetinaUrl   : 'http://www.poidb.com/images/Tick.gif',
            iconSize        : [24, 24],
            iconAnchor      : [12, 12],
            //popupAnchor: [-3, -76],
            //shadowUrl: 'my-icon-shadow.png',
            //shadowRetinaUrl: 'my-icon-shadow@2x.png',
            //shadowSize: [68, 95],
            //shadowAnchor: [22, 94]
        })
    });
    self.markerLayers[portal.guid] = portalCellMarker;
    portalCellMarker.addTo(self.markerLayersGroup);
};


window.plugin.londonCellCategorist._handlePortalSelected = function(data) {
    var self = this;

    var guid  = data.selectedPortalGuid;
    var p     = window.portals[guid];
    var popup = null;

    console.dir(p);

    var $popupTitle = $('<h4>Select a cell:</h4>');
    var $popupList = $('<select></select>');
    $popupList.append($('<option selected>Select a cell</option>'));
    for(var i = 0; i < self.cellList.length; i++) {
        var cell = self.cellList[i];
        var $cell = $('<option></option>')
            .val(cell.id)
            .text(cell.title);
        $popupList.append($cell);
    }

    var $popupButton = $('<button>Done</button>');
    $popupButton.click(function() {
        var selectedCell = $popupList.val();
        if(selectedCell != '') {
            var data = {
                guid        : guid,
                title       : p.options.data.title,
                lat         : p.options.data.latE6,
                lng     	: p.options.data.lngE6,
                cell        : selectedCell
            };

            var options = { uri: 'portal/create', data: data };

            if(self.portalMap[guid])
                options = { uri: 'portal/update/' + guid, data: { cell: selectedCell } };

            self.requestApi('POST', options.uri, options.data, function(err, portal) {
                if(err) { console.dir(err); return alert('Updating portal cell failed!'); }
                //window.portals[guid].options.data.cell = selectedCell;
                self.addPortal(portal);

                window.map.closePopup(popup);
            });
        }
    });

    var $popupContent = $('<div></div>');
    $popupContent.append($popupTitle);
    $popupContent.append($popupList);
    $popupContent.append($popupButton);

    popup = L.popup()
        .setLatLng(p.getLatLng())
        .setContent($popupContent[0])
        .openOn(window.map);
};

var setup = function() {
    var self = window.plugin.londonCellCategorist;

    //
    // Setup UI
    self.markerLayersGroup = new L.LayerGroup();
    window.addLayerGroup('London Cell Categorist', self.markerLayersGroup, true);

    //
    // Bind Listeners
    window.addHook('mapDataRefreshEnd', function() {

        // Get Cell list
        self.requestApi('GET', 'cell/list', function(err, cells) {
            if(err) return alert('Could not retrieve cell list: ' + err.toString());
            self.cellList = cells;
        });
        
        // Get Portal list
        self.requestApi('GET', 'portal/list', function(err, portals) {
            if(err) { console.dir(err); return alert('Could not retrieve portal list: ' + err.toString()) };
            console.debug('Got Portals: ', portals.length);
            for(var i = 0; i <= portals.length; i++) {
              self.addPortal(portals[i]);
            }
        });
    });
    
    window.addHook('portalSelected', function(data) {
        self._handlePortalSelected(data);
    });
};
    
// PLUGIN END //////////////////////////////////////////////////////////


setup.info = plugin_info; //add the script info data to the function as a property
if(!window.bootPlugins) window.bootPlugins = [];
window.bootPlugins.push(setup);
// if IITC has already booted, immediately run the 'setup' function
if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);
